/**
 * Gantt-panel with more features activated out of the box.
 */
Ext.define('Gnt.lib.GanttPanel', {
    extend:'Gnt.panel.Gantt',
    requires: [
        'Gnt.plugin.taskeditor.TaskEditor',
        'Gnt.plugin.TaskContextMenu',
        'Gnt.plugin.DependencyEditor',
        'Sch.plugin.TreeCellEditing'
    ],

    defaultPlugins: [
        // enables task editing by double clicking, displays a window with fields to edit
        'gantt_taskeditor',
        // enables double click dependency editing
        'gantt_dependencyeditor',
        // shows a context menu when right clicking a task
        'gantt_taskcontextmenu',
        // column editing
        'scheduler_treecellediting'
    ],

    dependencyViewConfig: {
        overCls : 'dependency-over'
    },

   initComponent: function() {
       var me = this;

       me.plugins = [].concat(me.plugins || []);

       // only add plugins not already there :)
       // rebuilt to not use private addPlugin()
       Ext.each(me.defaultPlugins, function(plugin) {
           if (!me.hasPlugin(plugin)) me.plugins.push(plugin);
       });

       me.callParent();
   },

    // replacement for findPlugin, to also work before init
    hasPlugin: function(ptype) {
        return Ext.Array.some(this.plugins, function(plugin) {
            return (plugin === ptype || plugin.ptype === ptype);
        });
    }
});
