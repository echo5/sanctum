# Preview all emails at http://localhost:3000/rails/mailers/task_mailer
class TaskMailerPreview < ActionMailer::Preview
	def email_comment
	  user = User.find(1)
	  comment = TaskComment.find(8)
	  TaskMailer.email_comment(comment, user)
	end
end
