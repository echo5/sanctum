require 'test_helper'

class PortfolioTagsControllerTest < ActionController::TestCase
  setup do
    @portfolio_tag = portfolio_tags(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:portfolio_tags)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create portfolio_tag" do
    assert_difference('PortfolioTag.count') do
      post :create, portfolio_tag: {  }
    end

    assert_redirected_to portfolio_tag_path(assigns(:portfolio_tag))
  end

  test "should show portfolio_tag" do
    get :show, id: @portfolio_tag
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @portfolio_tag
    assert_response :success
  end

  test "should update portfolio_tag" do
    patch :update, id: @portfolio_tag, portfolio_tag: {  }
    assert_redirected_to portfolio_tag_path(assigns(:portfolio_tag))
  end

  test "should destroy portfolio_tag" do
    assert_difference('PortfolioTag.count', -1) do
      delete :destroy, id: @portfolio_tag
    end

    assert_redirected_to portfolio_tags_path
  end
end
