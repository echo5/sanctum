class CreateTaskLabels < ActiveRecord::Migration
  def change
    create_table :task_labels do |t|
    	t.string :name
    	t.string :color
    end
  end
end
