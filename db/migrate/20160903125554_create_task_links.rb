class CreateTaskLinks < ActiveRecord::Migration
  def change
    create_table :task_links do |t|
      t.references :source, index: true, on_delete: :cascade
      t.references :target, index: true, on_delete: :cascade
      t.string  :link_type

      t.timestamps null: false
    end
  end
end