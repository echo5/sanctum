class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
		t.string :title
		t.string :slug, :unique => true
		t.string :description
		t.text :content, :limit => 4294967295
		t.boolean :is_published

      t.timestamps null: false
    end
  end
end
