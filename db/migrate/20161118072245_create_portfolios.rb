class CreatePortfolios < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|
		t.string :title
		t.string :slug
		t.text :description
		t.attachment :image
		t.text :content, :limit => 4294967295
		t.boolean :is_published, :default => nil
		t.integer :order, :default => 0

	  t.timestamps null: false
    end
  end
end
