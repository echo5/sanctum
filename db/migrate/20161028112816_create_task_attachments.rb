class CreateTaskAttachments < ActiveRecord::Migration
  def change
    create_table :task_attachments do |t|
		t.attachment :file
	    t.references :task, index: true, foreign_key: true
      	t.timestamps null: false
    end
  end
end
