class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.belongs_to :project, index: true
      t.string :name
      t.date :start_date
      t.date :end_date
      t.integer :duration
      t.date :planned_start
      t.date :planned_end
      t.float :progress
      t.integer :sortorder
      t.string :task_type
      t.integer :task_label_id
      t.integer :parent_id
      t.boolean :is_complete, :default => 0
      t.boolean :in_gantt, :default => 0

      t.timestamps
    end
  end
end