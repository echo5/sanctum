class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.attachment :image
      t.text :content, :limit => 4294967295
      t.references :user, index: true, foreign_key: true
      t.references :post_category, index: true, foreign_key: true
      t.boolean :is_published

      t.timestamps
    end
  end
end
