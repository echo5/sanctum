class CreateJoinTablePortfoliosPortfolioTags < ActiveRecord::Migration
  def change
    create_join_table :portfolios, :portfolio_tags do |t|
    	t.references :portfolio, index: {:name => "index_portfolio_id"}
    	t.references :portfolio_tag, index: {:name => "index_portfolio_tag_id"}
      # t.index [:portfolio_id, :portfolio_tag_id]
      # t.index [:portfolio_tag_id, :portfolio_id]
    end
  end
end
