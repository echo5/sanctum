class CreatePortfolioTags < ActiveRecord::Migration
  def change
    create_table :portfolio_tags do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
