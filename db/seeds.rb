# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Gantt example data
# if Task.gantt_data.empty?
#   p  = Project.create(name: "Project #1")
#   t1 = Task.create(name: "Task #1", start_date: Date.today - 2.days, duration:5, progress: 1, parent: 0, project_id: p.id)
#   t2 = Task.create(name: "Task #2", start_date: Date.today - 2.days, duration:1, progress: 0.4, parent: 0, project_id: p.id)
#   t3 = Task.create(name: "Task #2.1", start_date: Date.today - 1.day, duration:2, progress: 1, parent: t2.id, project_id: p.id)
#   t4 = Task.create(name: "Task #2.2", start_date: Date.today - 1.day, duration:3, progress: 0.8, parent: t2.id, project_id: p.id)
#   t5 = Task.create(name: "Task #2.3", start_date: Date.today, duration:4, progress: 0.2, parent: t2.id, project_id: p.id) 
#   p.tasks << [t1, t2, t3, t4, t5]

#   TaskLink.create(source: t1, target: t2, link_type:"1", project: p)
#   TaskLink.create(source: t2, target: t3, link_type:"0", project: p)
#   TaskLink.create(source: t3, target: t4, link_type:"1", project: p)  
# end

# User seeds
emails = ["joshua@echo5webdesign.com", "tom@sanctumdesigngroup.com", "hannah@sanctumdesigngroup.com"]

emails.each do |email|
  user = User.find_or_create_by(email: email)
  if user.password.nil?
    user.password = 'sanctump455'
    user.password_confirmation = 'sanctump455'
    user.save
  end
end

[:admin, :member].each do |role|
  Role.find_or_create_by( name: role )
end

emails.each do |email|
  user = User.find_by email: email
  user.add_role :admin
end