module ApplicationHelper
	def distance_in_human_time(time)
	  # return "tomorrow" if Date.parse(time).tomorrow?
	  # return "yesterday" if time.yesterday?
	  distance_of_time_in_words_to_now(time)
	end

	def nav_link(link_text, link_path, sub_menu = nil)
	  class_name = request.fullpath.start_with?(link_path) ? 'nav-item active' : 'nav-item'

	  content_tag(:li, :class => class_name) do
	    concat(link_to link_text, link_path) 
	    if defined?(sub_menu) && !sub_menu.blank?
	    	concat(content_tag(:ul) do
		    	sub_menu
			end)
	    end
	  end
	end

	def title(page_title)
	  content_for :title, page_title.to_s
	end

	def meta_description(desc = nil)
	  if desc.present?
		content_for(:meta_description, tag(:meta, name: 'description', content: desc ))
 	  end
	end

	def body_class(classes)
	  content_for :body_class, classes.to_s
	end

	def namespace(string)
	  content_for :namespace, string.to_s
	end
end
