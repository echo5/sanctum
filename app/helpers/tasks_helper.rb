module TasksHelper
	def hex_to_rgba hex, opacity
	  a = ( hex.match /#(..?)(..?)(..?)/ )[1..3]
	  a.map!{ |x| x + x } if hex.size == 4
	  "rgba(#{a[0].hex},#{a[1].hex},#{a[2].hex},#{opacity})"
	end
end
