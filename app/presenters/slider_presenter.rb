class SliderPresenter

  def self.for
    # An array can also be returned if the presenter should be applied
    # to multiple shortcodes, e.g. [:gallery, :enhanced_gallery]
    :slider
  end

  def initialize(attributes, content, additional_attributes)
    @content = content
  end

  def content
    @content
  end

  def attributes
  end

end