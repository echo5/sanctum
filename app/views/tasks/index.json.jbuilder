json.tasks(@tasks) do |task|
  json.extract! task, :id, :name, :is_complete, :start_date, :end_date, :user_ids, :project_id
end
json.users(@users) do |user|
  json.extract! user, :id, :name, :email, :avatar, :color
end
json.projects(@projects) do |project|
  json.extract! project, :id, :name
end
json.comments(@task_comments) do |comment|
  json.extract! comment, :id, :content, :task
  json.created_at l(comment.created_at)
  json.user comment.user, :id, :name, :email, :avatar
end
