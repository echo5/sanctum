json.extract! portfolio_tag, :id, :created_at, :updated_at
json.url portfolio_tag_url(portfolio_tag, format: :json)