Ext.define('Sanctum.Gantt.TemplateTask', {
    extend : 'Gnt.model.Task',
    startDateField: 'start_date',
    endDateField: 'end_date',
    durationField: 'duration',
    nameField: 'name',
    idProperty: 'id',
    baselineStartDateField: 'planned_start',
    baselineEndDateField: 'planned_end',
    // parentIdProperty: 'parent_id',
    // fields : [
    //     { name : 'start_date', mapping : 'StartDate' },
    //     { name : 'end_date', mapping : 'EndDate' },
    //     { name : 'index',    mapping : 'OrderNumber' },
    //     { name : 'depth',    mapping : 'Depth' },
    //     { name : 'children', mapping : 'Children' },
    //     { name : 'expanded', defaultValue : true },
    //     { name : 'Priority' },
    //     { name : 'leaf',  type: 'bool', mapping: 'IsLeaf' },
    //     { name : 'PercentDone', mapping : 'Progress' }
    // ]

    fields : [
        { name : 'project_id', type: 'int', useNull: true, persist: true }
    ],

    validations : [
        {type: 'presence',  field: 'project_id'}
    ]
});

/**
 * This example shows both the current plan and the original baseline.
 */
Ext.define('Gnt.examples.baseline.view.Gantt', {
    extend : 'Gnt.lib.GanttPanel',

    requires : [
        'Gnt.column.Name',
        // 'Gnt.column.BaselineStartDate',
        // 'Gnt.column.BaselineEndDate',
        'Sch.plugin.Pan'
    ],

    // title : 'Baseline demo',

    // enables showing baseline for tasks
    enableBaseline  : true,
    // show baselines from start
    baselineVisible : true,

    viewPreset        : 'weekAndDayLetter',
    rowHeight         : 40,
    eventBorderWidth  : 0,
    highlightWeekends : true,

    width: 400,
    height: 600,

    listeners:{ 
        itemadd:function(records , index , node , eOpts ) { 
           console.log(records); 
           console.log(index); 
           console.log(node); 
           console.log(eOpts); 
        } 
    }, 

    columns : [
        {
            xtype : 'namecolumn',
            width : 200
        },
        // {
        //     // column with baseline start date of a task
        //     xtype : 'baselinestartdatecolumn'
        // },
        // {
        //     // column with baseline end date of a task
        //     xtype : 'baselineenddatecolumn'
        // }
    ],

    plugins : [
        {
            // plugin that allows gantt to be panned by dragging the view around
            ptype : 'scheduler_pan'
        }
    ],

    initComponent : function() {
        var me = this;

        var taskStore = Ext.create('Gnt.data.TaskStore', {
            autoSync: true,
            model : 'Sanctum.Gantt.TemplateTask',
            proxy : {

                type   : 'rest',
                url    : '/tasks',
                reader : {
                    type  : 'json',
                }
            }
        });

        var dependencyStore = Ext.create('Gnt.data.DependencyStore', {
            //autoLoad : true,
            autoSync: true,
            proxy    : {
                type   : 'rest',
                url    : '/task_links'
            }
        });

        var resourceStore = Ext.create('Gnt.data.ResourceStore', {
            //autoLoad : true,
            autoSync: true,
            proxy    : {
                type   : 'rest',
                url    : '/users'
            }
        });

        // var assignmentStore = Ext.create('Gnt.data.AssignmentStore', {
        //     //autoLoad : true,
        //     autoSync: true,
        //     proxy    : {
        //         type   : 'rest',
        //         url    : '/tasks_users'
        //     }
        // });

        Ext.apply(me, {
            taskStore       : taskStore,
            dependencyStore : dependencyStore,
            resourceStore : resourceStore,
            // assignmentStore : assignmentStore,

            tools : [
                {
                    xtype        : 'button',
                    text         : 'Show baseline task bars',
                    enableToggle : true,
                    pressed      : true,
                    style        : 'margin-right: 5px',
                    handler      : function() {
                        // toggle baseline visibility
                        me.toggleBaseline();
                    }
                },
                {
                    xtype   : 'button',
                    text    : 'Set new baseline',
                    handler : function() {
                        // adjust baseline for all nodes to their current start and end dates
                        taskStore.getRoot().cascadeBy(function(node) {
                            node.beginEdit();
                            node.set('BaselineStartDate', node.getStartDate());
                            node.set('BaselineEndDate', node.getEndDate());
                            node.endEdit();
                        });
                    }
                }
            ]
        });

        me.callParent();

        dependencyStore.load();
    }
});

Ext.application({
    name : 'Gnt.examples.baseline.view.Gant',

    requires : [
        'Gnt.examples.baseline.view.Gantt'
    ],

    launch : function() {
        Ext.QuickTips.init();

        Ext.create('Ext.tab.Panel', {
            renderTo: 'gantt',
            layout  : 'border',
            width: '100%',
            height: '100%',
            items : [
                Ext.create('Gnt.examples.baseline.view.Gantt', {
                    // layout : 'border',
                    // region : 'center',
                })
            ]
        });
    }
});

// Ext.onReady(function() {
//     Ext.create('Gantt', {
//         renderTo : 'gantt-container'
//     });
// });