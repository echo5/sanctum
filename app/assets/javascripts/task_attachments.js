function initDropzone() {
	Dropzone.autoDiscover = false;

	// Dropzone init
	$("#new_task_attachment").dropzone({
		maxFilesize: 50,
		paramName: "task_attachment[file]",
		addRemoveLinks: true,
		previewsContainer: "#task-attachments-previews",
		clickable: '#attachment-add, #new_task_attachment',
		thumbnailWidth: 150,
		thumbnailHeight: 100,
		dictRemoveFile: '',
		success: function(file, response){
			$(file.previewTemplate).attr('id', 'task-attachment-' + response.id).data('attachment', response.id);
			$(file.previewElement).addClass("dz-success");
		},
		removedfile: function(file){
			var id = $(file.previewTemplate).data('attachment'); 
			$.ajax({
				type: 'DELETE',
				url: '/attachments/' + id,
				success: function(data){
					// console.log(data.message);
				}
			});
		},
		previewTemplate: 	'<div class="dz-preview dz-file-preview task-attachment" id="task-attachment-">' +
							  '<div class="dz-details task-attachment-info">' +
							    '<img data-dz-thumbnail class="tooltipped" data-tooltip="data-dz-name" data-position="left" />' +
							    // '<div class="dz-filename"><span data-dz-name></span></div>' +
							    // '<div class="dz-size" data-dz-size></div>' +
							  '</div>' +
							  '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
							  '<div class="dz-success-mark"></div>' +
							  '<div class="dz-error-mark"></div>' +
							  '<div class="dz-error-message"><span data-dz-errormessage></span></div>' +
							'</div>',
	});	

	// Drag into dropzone
	var droppable = $('#new_task_attachment'),
		modal = $('#modal-task'),
	    lastenter;

    function fixScroll() {
      var top = modal.scrollTop();
      droppable.css('position','absolute');
      droppable.css('top',top + 'px');
    };
	    
	droppable.on("dragenter", function (event) {
	    lastenter = event.target;
	    droppable.addClass("dz-dragenter");            
	});

	droppable.on("dragleave", function (event) {
	    if (lastenter === event.target) {
	        droppable.removeClass("dz-dragenter");
	    }
	});
	droppable.on("drop", function (event) {
	    droppable.removeClass("dz-dragenter");            
	});

	// Drag into window
	var lastTarget = null;
	window.addEventListener("dragenter", function(e)
	{
	    lastTarget = e.target;
	    $('body').addClass('dz-dragover');
	    fixScroll();
	});

	window.addEventListener("dragleave", function(e)
	{
	    if(e.target === lastTarget)
	    {
	    	$('body').removeClass('dz-dragover');
	    }
	});


}

$(document).ready(function(){

});