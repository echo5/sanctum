/** 
 * Treegrid columns
 */
gantt.config.columns = [
    {name:"text", label:"Task name", width:"140", tree:true,
        template: function(task) {
            html = task.text 
            if (task.unread_comments > 0) {
              html += '<span class="task-comments-unread">' + task.unread_comments + '</span>';
            }
            return html;
        }
     },
    {name:"progress", label:"Progress", width:80, align: "center",
        template: function(task) {
            if (task.type == 'project') {
              return '';
            }
            if (task.progress == undefined) {
              return '0%';
            }
            return parseInt(Math.round(task.progress*100)) + "%";
        }
    },
];