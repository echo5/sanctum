// Add weekends
gantt.templates.task_cell_class = function(task, date){
  if(!gantt.isWorkTime(date))
    return "week_end";
};

// Add row classes
gantt.templates.grid_row_class=function(start, end, task) {
  return 'task-type-' + task.type + ' task-level-'+ task.$level;
};

// Add task classes
gantt.templates.task_class = function (start, end, task) {
  var classes = [];
  if (task.assignee_id) {
    classes.push('resource-' + task.assignee_id);
  }
  if (task.planned_end) {
    classes.push('has-baseline');
    if (end.getTime() > task.planned_end.getTime()) {
      classes.push('overdue');
    }
  }
  return classes.join(' ');
};