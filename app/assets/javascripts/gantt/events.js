/**
 * Remove unread comments marker on gantt
 */
$(window).on("closeTaskModal", function(event, task_id) {
  gantt.removeUnreadCommentsMarker(task_id);
});
gantt.removeUnreadCommentsMarker = function(task_id) {
  task = gantt.getTask(task_id);
  task.unread_comments = 0;
  gantt.refreshData();
};

/**
 * New task attributes
 */
gantt.attachEvent("onBeforeTaskAdd", function (id, task) {
  var parent = gantt.getTask(task.parent);
  task.project_id = parent.project_id;
  task.in_gantt = true;
  task.sortorder = 100;
});

/**
 * Update gantt after task form submitted
 */
$(window).on("onAfterTaskFormSubmit", function(event, task_id, attribute, value) {
	// Update task in gantt
	var task = {};
	if(gantt.isTaskExists(task_id)) {
	    task = gantt.mixin({}, gantt.getTask(task_id));
	}
	
	// Change attributes to fit gantt
	switch(attribute) {
		case 'name':
			attribute = 'text';
			break;
	}

	task[attribute] = value;

	// Update duration if date picker
	if (attribute == 'start_date'  || attribute == 'end_date') {
	  task.duration = gantt.calculateDuration(task.start_date, task.end_date);
	} 

	// Don't update database since it's already updated
	gantt._silent_mode = true;
	gantt.updateTask(task.id, task);
	gantt._silent_mode = false;

	gantt.refreshData();
	return task;
});