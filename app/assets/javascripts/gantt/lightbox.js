
/**
 * Built in task types
 */
var taskTypes = [
    { key:'task', label: 'Task' },
    { key:'project', label: 'Project' },
    { key:'milestone', label: 'Milestone' },
];

/**
 * Convert Date Object string to yyyy-mm-dd
 */
function dateStringToFormat(dateString) {
    if (dateString !== null) {
        var d = new Date(dateString);
        var curr_date = ("0" + d.getDate()).slice(-2)
        var curr_month = ("0" + (d.getMonth() + 1)).slice(-2)
        var curr_year = d.getFullYear();
        return curr_year + "-" + curr_month + "-" + curr_date;
    }
    return '';
}

// Lightbox layout
gantt.getLightbox = function(task, type){

    if(type === undefined)
        type = this.getLightboxType();

    if (!this._lightbox || this.getLightboxType() != this._get_safe_type(type)){
        this._lightbox_type = this._get_safe_type(type);
      }

    if (task.id)
        gantt._lightbox_id = task.id;

    // Start content
    var html = '<div class="modal-content task-type-'+type+'">';
    // Header
    // html += '<h4>'+task.text+'</h4>'
    // Form
    html += '<form class="form-task-edit" id="form-task-edit">';
    html += '<div class="row">';
      html += '<div class="input-field col s12">';
      html += '<input name="text" type="text" value="'+task.text+'">';
      html += '<label for="text">Name</label>';
      html += '</div>';
    html += '</div>';  
    html += '</form">';
    // End content
    html += '</div>';
    // Buttons
    html += '<div class="modal-footer">';
    // html += '<span class="btn btn-flat modal-action waves-effect left btn-task-delete waves-red" id="btn-task-delete">Delete</span>'
    html += '<span class="btn modal-action waves-effect right btn-task-save waves-light" id="btn-task-save">Save</span>'
    html += '<span class="btn-flat modal-action waves-effect right btn-task-cancel waves-light" id="btn-task-cancel">Cancel</span>'
    html += '</div>';


    var modal = $('#modal-task-edit');
    modal.html(html);
    modal.openModal({
        dismissible: false,
    });
    Materialize.updateTextFields();

    return this._lightbox;
};

/**
 * Prepare lightbox
 */
gantt.showLightbox=function(id){
    if (!id || gantt._is_readonly(this.getTask(id))) return;
    if (!this.callEvent("onBeforeLightbox",[id])) return;

    var task = this.getTask(id);

    var box = this.getLightbox(task, this._get_safe_type(task.type));
    // this._fill_lightbox(id,box);
    this.callEvent("onLightbox",[id]);
};

/**
 * Get lightbox values on save
 */
gantt.getLightboxValues=function(){
    var task = {};

    if(gantt.isTaskExists(this._lightbox_id)) {
        task = this.mixin({}, this.getTask(this._lightbox_id));
    }

    $('#form-task-edit *').filter(':input').each(function(){
        // Datepicker
        if ($(this).hasClass('datepicker')) {
            var picker = $(this).pickadate('picker');
            if (picker.get('select') !== null) {
              task[$(this).attr('name')] = picker.get('select').obj;
            }
        } 
        // Default
        else {
           task[$(this).attr('name')] = $(this).val(); 
       }
    });

    return task;
};

/**
 * Hide lightbox
 */
gantt.hideLightbox=function(){
    this._lightbox_id=null;
    $('#modal-task-edit').closeModal();
    this.callEvent("onAfterLightbox",[]);
};

/**
 * Hide action buttons
 */
$(document).on('click','#btn-task-save', function(){
    gantt._save_lightbox();
});
$(document).on('click','#btn-task-cancel', function(){
    gantt._cancel_lightbox();
});
$(document).on('click','#btn-task-delete', function(){
    if(!gantt.callEvent("onLightboxDelete", [gantt._lightbox_id]))
        return;

    if(gantt.isTaskExists(gantt._lightbox_id)){
        gantt.$click.buttons["delete"](gantt._lightbox_id);
    }else{
        gantt.hideLightbox();
    }
});
gantt.$click={
    buttons:{
        "edit":function(id){
            gantt.showLightbox(id);
        },
        "delete":function(id){
            gantt.confirmDelete(id);
        }
    }
};


/**
 * Lightbox actions
 */
gantt._save_lightbox=function(){
    var task = this.getLightboxValues();
    if(!this.callEvent("onLightboxSave", [this._lightbox_id, task, !!task.$new]))
        return;

    if (task.$new){
        delete task.$new;
        this._replace_branch_child(this.getParent(task.id), task.id);

        this.addTask(task);
    }else if(this.isTaskExists(task.id)){
        this.mixin(this.getTask(task.id), task, true);
        this.updateTask(task.id);
    }
    this.refreshData();

    // TODO: do we need any blockable events here to prevent closing lightbox?
    this.hideLightbox();
};
gantt._cancel_lightbox=function(){
    var task = this.getLightboxValues();
    this.callEvent("onLightboxCancel",[this._lightbox_id, task.$new]);
    if(gantt.isTaskExists(task.id) && task.$new){
        this._deleteTask(task.id, true);
    }

    this.refreshData();
    this.hideLightbox();
};
gantt.confirmDelete=function(id) {
    $('#modal-task-delete').openModal();

    $(document).on('click','#btn-task-delete-confirm', function(){
        $('#modal-task-delete').closeModal();

        if(!gantt.isTaskExists(id)){
            gantt.hideLightbox();
            return;
        }

        var task = gantt.getTask(id);
        if(task.$new){
            gantt._deleteTask(id, true);
            gantt.refreshData();
        }else{
            gantt.deleteTask(id);
        }

        gantt.hideLightbox();
    });
}

