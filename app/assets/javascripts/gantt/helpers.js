// Helper functions
// Set option for select item
function setOption(selectElement, value) {
    var options = selectElement.options;
    for (var i = 0, optionsLength = options.length; i < optionsLength; i++) {
        if (options[i].value == value) {
            selectElement.selectedIndex = i;
            return true;
        }
    }
    selectElement.selectedIndex = 0;
    return false;
} 
// Get root parent (project) ID
function getRootParent(task_id) {
  var root_id = task_id;
  var parent_id = gantt.getParent(task_id);
  if (parent_id) {
    return getRootParent(parent_id);
  }
  return root_id;
}