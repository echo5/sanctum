// Add baseline
gantt.addTaskLayer(function draw_planned(task) {
  if (task.planned_start && task.planned_end) {
    var sizes = gantt.getTaskPosition(task, task.planned_start, task.planned_end);
    var el = document.createElement('div');
    el.className = 'baseline';
    el.style.left = sizes.left + 'px';
    el.style.width = sizes.width + 'px';
    el.style.top = sizes.top + gantt.config.task_height  + 13 + 'px';
    return el;
  }
  return false;
});
gantt.templates.rightside_text = function (start, end, task) {
  if (task.planned_end) {
    if (end.getTime() > task.planned_end.getTime()) {
      var overdue = Math.ceil(Math.abs((end.getTime() - task.planned_end.getTime()) / (24 * 60 * 60 * 1000)));
      var text = "<span class='baseline-overdue'>Overdue: " + overdue + " days</span>";
      return text;
    }
  }
};
gantt.attachEvent("onTaskLoading", function(task){
  task.planned_start = gantt.date.parseDate(task.planned_start, "xml_date");
  task.planned_end = gantt.date.parseDate(task.planned_end, "xml_date");
  return true;
});
