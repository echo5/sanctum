/**
 * Today line
 */
var today = new Date();
var date_to_str = gantt.date.date_to_str(gantt.config.task_date);
var todayString = today.getFullYear() + '-' +today.getMonth()+1 + '-' + today.getDate();
gantt.addMarker({
  start_date: today,
  css: "today",
  text: "Today",
  title:"Today: "+ date_to_str(today)
});