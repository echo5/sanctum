function clickTaskButton(id, action) {
  switch (action) {
    case "add":
      gantt.createTask(null, id);
      break;
    case "delete":
      gantt.confirmDelete(id);
      break;
    case "comments":
      showTaskModal(id);
      break;
  }
}

gantt._on_dblclick = function(e) {
  e = e || window.event;
  var trg = e.target || e.srcElement;
    var id = gantt.locate(e);
  var res = !gantt.checkEvent("onTaskDblClick") || gantt.callEvent("onTaskDblClick", [id, e]);
  if(res){
    var default_action = gantt._find_ev_handler(e, trg, gantt._dbl_click, id);
    if(!default_action)
      return;

    if (id !== null && gantt.getTask(id)){
      if(res && gantt.config.details_on_dblclick){
        showTaskModal(id);
      }
    }
  }
};

gantt.templates.leftside_text = function (start, end, task) {
  if (task.$level < 2) {
    return '<div class="gantt-buttons">'
      +'<a class="btn-small btn-floating btn-delete"><i class="ion-trash-b" onclick="clickTaskButton('+task.id+', \'delete\')"></i></a>'
      +'<a class="btn-small btn-comments btn-floating" onclick="clickTaskButton('+task.id+', \'comments\')"><i class="ion-chatbubbles"></i></a>'
      +'<a class="btn-small btn-add btn-floating" onclick="clickTaskButton('+task.id+', \'add\')"><i class="ion-android-add"></i></a>'
    +'</div>';
  }
  return '<div class="gantt-buttons">'
    +'<a class="btn-small btn-delete btn-floating" onclick="clickTaskButton('+task.id+', \'delete\')"><i class="ion-trash-b"></i></a>'
    +'<a class="btn-small btn-comments btn-floating" onclick="clickTaskButton('+task.id+', \'comments\')"><i class="ion-chatbubbles"></i></a>'
  +'</div>';
};