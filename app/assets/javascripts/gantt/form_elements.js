/**
 * Create a selected options dropdown
 */
function renderSelectOptions(name, options, selectedValue, defaultOption, selectAttribute) {
  html = '<select name="' + name + '" ' + selectAttribute + '>';
  var selected = '';
  if (defaultOption && defaultOption.length) {
    if (selectedValue == null) {
      selected = 'selected';
    }
    html += '<option value="" disabled ' + selected +'>';
    html += defaultOption;
    html += '</option>';
  }
  $.each(options, function() {
    console.log(selectedValue);
    selected = '';
    if ($.isArray(selectedValue) && $.inArray( $(this)[0].value, selectedValue ) !== -1) {
      selected = 'selected'
    }
    else if (selectedValue == $(this)[0].value) {
      selected = 'selected';
    }
    html += '<option value="' + $(this)[0].value + '" ' + selected +'>';
    html += $(this)[0].label;
    html += '</option>';
  });
  html += '</select>';
  return html;
}


 // Form templates
 gantt.form_blocks["datepicker"] = {
   render:function(sns) {
       return '<div class="input-field"><input type="date" class="datepicker"></div>';
   },
   set_value:function(node,value,ev){
     var el = $(this.form_blocks.datepicker._get_input(node));
     var input = el.pickadate({
       onStart: function ()
       {
           this.set('select', '' )
       },
     });
     var picker = input.pickadate('picker');
     picker.set('select', value);
   },
   get_value:function(node,ev){
     var el = $(this.form_blocks.datepicker._get_input(node));
     var picker = el.pickadate('picker');
     if (picker.get('select') !== null) {
       return picker.get('select').obj;
     }
     return "";
   },
   focus:function(node){
     var a = this.form_blocks.datepicker._get_input(node);
     gantt._focus(a, true);
   },
   _get_input: function(node){
     return node.querySelector("input");
   }
 };
 gantt.form_blocks["textarea"] = {
    render:function(sns){
      var height=(sns.height||"130")+"px";
      return "<textarea class='materialize-textarea'></textarea>";
    },
    set_value:function(node,value,ev){
      this.form_blocks.textarea._get_input(node).value=value||"";
    },
    get_value:function(node,ev){
      return this.form_blocks.textarea._get_input(node).value;
    },
    focus:function(node){
      var a = this.form_blocks.textarea._get_input(node);
      gantt._focus(a, true);
     },
    _get_input: function(node){
      return node.querySelector("textarea");
    }
 };
 gantt.form_blocks["text"] = {
   render:function(sns){
     return "<div class='input-field'><input type='text' /></div>";
   },
   set_value:function(node,value,ev){
     this.form_blocks.text._get_input(node).value=value||"";
   },
   get_value:function(node,ev){
     return this.form_blocks.text._get_input(node).value;
   },
   focus:function(node){
     var a = this.form_blocks.text._get_input(node);
     gantt._focus(a, true);
   },
   _get_input: function(node){
     return node.querySelector("input");
   }
 };
 gantt.form_blocks["select"] = {
     render:function(sns){
         var height=(sns.height||"23")+"px";
         var html="<div class='input-field' style='height:"+height+";'><select style='width:100%;'>";
         for (var i=0; i < sns.options.length; i++)
             html+="<option value='"+sns.options[i].key+"'>"+sns.options[i].label+"</option>";
         html+="</select></div>";
         return html;
     },
     set_value:function(node,value,ev,sns){
         var select = this.form_blocks.select._get_input(node);
         if (!select._dhx_onchange && sns.onchange) {
             select.onchange = sns.onchange;
             select._dhx_onchange = true;
         }
         if (typeof value == "undefined")
             value = (select.options[0]||{}).value;
         select.value=value||"";
     },
     get_value:function(node,ev){
         return this.form_blocks.select._get_input(node).value;
     },
     focus:function(node){
         var a=node.firstChild; gantt._focus(a, true);
     },
     _get_input: function(node){
       return node.querySelector("select");
     }
 };

