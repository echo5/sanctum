$(function(){

	if($('.barba-container').length) {
			/**
			 * Portfolio index transition
			 */ 
			var PortfolioTransition = Barba.BaseTransition.extend({
			  start: function() {
			    Promise
			      .all([this.newContainerLoading, this.fadeOut()])
			      .then(this.fadeIn.bind(this));
			  },

			  fadeOut: function() {
		    	var deferred = Barba.Utils.deferred();
			    $(this.oldContainer).velocity({ opacity: 0 }, {
			    	complete: function() {
					    deferred.resolve();
			    	},
			    	delay: 1400
		    	});
				return deferred.promise;
			  },

			  fadeIn: function() {
			    var _this = this;
			    var $el = $(this.newContainer);

			    $(this.oldContainer).hide();

			    $el.css({
			      visibility : 'visible',
			      opacity : 0
			    });

			    // Change body class
			    $('body').attr('class', bodyClasses);

			    document.body.scrollTop = 0;

			    $el.velocity({ opacity: 1 }, {
			    	duration: 400,
			    	complete: function() {
				      _this.done();
			    	}
			    });
			  }
			});

			/**
			 * Default transition
			 */ 
			var FadeTransition = Barba.BaseTransition.extend({
			  start: function() {
			    Promise
			      .all([this.newContainerLoading, this.fadeOut()])
			      .then(this.fadeIn.bind(this));
			  },

			  fadeOut: function() {
		    	var deferred = Barba.Utils.deferred();
			    $(this.oldContainer).velocity({ opacity: 0 }, {
			    	complete: function() {
					    deferred.resolve();
			    	},
			    	delay: 300
		    	});

				return deferred.promise;
			  },

			  fadeIn: function() {
			    var _this = this;
			    var $el = $(this.newContainer);

			    $(this.oldContainer).hide();

			    $el.css({
			      visibility : 'visible',
			      opacity : 0
			    });

			    // Change body class
			    $('body').attr('class', bodyClasses);

			    document.body.scrollTop = 0;

			    $el.velocity({ opacity: 1 }, {
			    	duration: 400,
			    	complete: function() {
				      _this.done();
			    	}
			    });
			  }
			});

			Barba.Pjax.getTransition = function() {
			  /**
			   * Here you can use your own logic!
			   * For example you can use different Transition based on the current page or link...
			   */

			   var transitionObj = FadeTransition;

			   if ($(lastClickEl).data('transition') == 'portfolio') {
			   	transitionObj = PortfolioTransition;
			   }
			   // console.log(Barba.HistoryManager.currentStatus().namespace);
			   // console.log(Barba.HistoryManager.prevStatus().namespace);
			   // if (Barba.HistoryManager.prevStatus().namespace === 'detail') {
			   //   transitionObj = ShrinkTransition;
			   // }

			   return transitionObj;

			};

			/**
			 *	Update body classes
			 */
			var bodyClasses;
			var originalFn = Barba.Pjax.Dom.parseResponse;
			Barba.Pjax.Dom.parseResponse = function(response) {
			    response = response.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', response);
			    bodyClasses = $(response).filter('notbody').attr('class');
			    return originalFn.apply(Barba.Pjax.Dom, arguments);
			};

			/**
			 *  Get clicked link
			 */
			var lastClickEl;
			Barba.Dispatcher.on('linkClicked', function(el) {
			  lastClickEl = el;
			});

			Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {

				// Swap active link
				$('#nav-mobile').find('.nav-item').removeClass('active');
				var link = currentStatus.url.split(window.location.origin)[1].substring(1);
				$('#nav-mobile').find('.nav-item a').each(function(){
					if ($(this).attr('href') == '/' + link) {
						$(this).parent().addClass('active');
					}
				});
			});

			/**
			 *  Tabs
			 */
			Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container) {
			  $('ul.tabs').tabs();
			});

			/**
			 *  Unbind scroll events
			 */
			Barba.Dispatcher.on('linkClicked', function(currentStatus, oldStatus, container) {
			  $(window).unbind('scroll');
			});



			Barba.Pjax.start();
	}

});