(function($) {
    "use strict";

	/**
	 * In viewport helper
	 * @return {Boolean} 
	 */
	$.fn.inView = function(buffer){

		buffer = typeof buffer !== 'undefined' ? buffer : 100;
	    var win = $(window);

	    var viewport = {
	        top : win.scrollTop(),
	        left : win.scrollLeft()
	    };
	    viewport.right = viewport.left + win.width() - buffer;
	    viewport.bottom = viewport.top + win.height() - buffer;

	    var bounds = this.offset();
	    bounds.right = bounds.left + this.outerWidth();
	    bounds.bottom = bounds.top + this.outerHeight();

	    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

	/**
	 * Custom animations
	 */
 	$.Velocity.RegisterEffect('transition.moveUp', {
 	    defaultDuration: 400,
 	    calls: [
 		    [ { opacity: [1, 0], translateY: [ 0, 140 ], translateZ: 0 } ]
 	    ],
 	});
 	$.Velocity.RegisterEffect('transition.moveRight', {
 	    defaultDuration: 1000,
 	    calls: [
 		    [ { translateX: [ '110%', '0' ], translateZ: 0 } ]
 	    ],
 	    reset: { translateX: '110%' }
 	});
 	$.Velocity.RegisterUI('transition.opacityOut', {
 	    defaultDuration: 1000,
 	    calls: [
 		    [ { opacity: [1,0], display: 'block'}, {display: 'block'} ]
 	    ],
 	    reset: { display: 'block' }
 	});

 	/**	
 	 * Animate objects
 	 */
	function animate(element, count, state) {

		var animationClass = state ? 'animated-' + state : 'animated';
		var animationName = element.data('animation');
		var animationCount = element.data('animation-loop');
		var animationDuration = element.data('animation-duration');
		var animationDelay = element.data('animation-delay');
		if (state) {
			animationName = element.data('animation-out');
		}

		if (animationName) {
			element.addClass(animationClass);
			element.velocity(animationName, { 
				delay: animationDelay,
				duration: animationDuration,
				begin: function(elements) {
					element.addClass('visible');
				},
				complete: function(elements) {
					count++;
					if (count < animationCount || animationCount === true) {
						animate(element, count);
					} else {
						// element.css('transform', '');
					}
				}
			});
		} else {
			function startAnimation() {
				element.addClass(animationClass);
			}
			setTimeout(startAnimation, animationDelay);
		}
	}

	/**
	 *  Animate elements inside viewport
	 */
	function animateInView(elements) {
		elements.each(function() {
			if (!$(this).hasClass('animated') && !$(this).hasClass('animated-out')) {
				if ($(this).inView()) {
					animate($(this), 0, null);
				}
			} else {
				if (!$(this).inView() && $(this).hasClass('reanimate')) {
					$(this).removeClass('animated');
				}
			}
		});
	}

	/**
	 * Animate out all elements
	 */ 
	function animateOutAllElements(elements, state) {
		elements.each(function() {
			if (!$(this).hasClass('animated-out') ) {
				animate($(this), 0, state);
			}
		});
	}
	
	/**
	 * Init animations
	 */
	function startAnimations() {
		var animationBlocks = $('.has-animation');
		animateInView(animationBlocks);
		$(window).scroll(function() {
			animateInView(animationBlocks);
		});
	}
	startAnimations();
	$( window ).on( "load", function() {
		startAnimations();
	});
	Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {
		startAnimations();
	});
	Barba.Dispatcher.on('linkClicked', function(currentStatus, oldStatus, container) {
	  animateOutAllElements($('.has-animation-out'), 'out');
	});

	/**
	 *  Parallax fxn for elements
	 */
	$.fn.parallax2 = function(momentum, axis) {
		momentum = typeof momentum !== 'undefined' ? momentum : '0.5';
		axis = typeof axis !== 'undefined' ? axis : 'y';
	    var scrollTop = $(window).scrollTop();
	    var offset = this.offset();
	    var moveValue = 0 - Math.round((offset.top - scrollTop) * momentum);
	    console.log(offset.top);
	    console.log(scrollTop);
	    console.log(moveValue);
	    // this.css('transform', 'translateY( '+  moveValue +'px)');
	    if (axis === "x") {
	    	this.velocity({
	    		translateX: moveValue + "px",
	    	}, { queue: false, duration: 0 });  
	    } 
	    else {
			this.velocity({
				translateY: moveValue + "px",
			}, { queue: false, duration: 0 });   	
	    }
	};

	function moveParallaxLayers() {
		$('.parallax-layer').each(function(){
			if ($(this).inView(0)) {
				var momentum = $(this).data('parallax-momentum');
				var axis = $(this).data('parallax-axis');
			    $(this).parallax2(momentum, axis);
			}
		});
	}

	function parallaxLayers() {
		$(window).on('scroll', function() {
			moveParallaxLayers();
		});
	}
 //    $(window).on('load', function(){
	// 	moveParallaxLayers();
	// 	$(window).scroll();
	// });


    $(document).ready(function(){
      $('.parallax').parallax();
    	moveParallaxLayers();
    });
    Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container) {
    	$('.parallax').parallax();
    	moveParallaxLayers();
		$('.parallax-layer').velocity({ opacity: 1 }, { duration: 300});
    	parallaxLayers();

    });


})(jQuery);