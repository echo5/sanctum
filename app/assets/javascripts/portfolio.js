/**
 * Init portfolio snapping
 */
function initPortfolio() {
  if($('#portfolio').length) {
    $.scrollify({
        section : ".portfolio-item",
        scrollbars:false,
        offset : 0,
        updateHash: false,
        before:function(i,panels) {
          $('.svg-frame').removeClass('animated');
          var ref = panels[i].attr("data-section-name");
          $(".pagination .active").removeClass("active");
          $(".pagination").find("a[href=\"#" + ref + "\"]").parent().addClass("active");
        },
        after: function(i, panels) {

        },
        afterRender:function() {
          var pagination = "<ul class=\"pagination\">";
          var activeClass = "";
          $(".portfolio-item").each(function(i) {
            activeClass = "";
            if(i===0) {
              activeClass = "active";
            }
            pagination += "<li class=\"" + activeClass + "\"><a href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-name").charAt(0).toUpperCase() + $(this).attr("data-section-name").slice(1) + "</span></a></li>";
          });

          pagination += "</ul>";

          $(".portfolio").append(pagination);
          $('#portfolio').velocity({opacity: 1});
        }
    });
    
    $(".pagination a").on("click",$.scrollify.move);
  }
}

/**
 * Portfolio transitions
 */
var Portfolio = Barba.BaseView.extend({
  namespace: 'portfolio',
  onEnter: function() {
  },
  onEnterCompleted: function() {
    initPortfolio();
  },
  onLeave: function() {
  },
  onLeaveCompleted: function() {
    $.scrollify.destroy();
    $('body').css('overflow', 'visible');
  }
});
Portfolio.init();

var PortfolioSingle = Barba.BaseView.extend({
  namespace: 'portfolioSingle',
  onEnter: function() {
  },
  onEnterCompleted: function() {
    var viewportHeight = $( window ).height();
    $('body.portfolio-single').velocity('scroll',{
      duration: 500, 
      offset: viewportHeight * 0.25,
      delay: 500,
    });
  },
  onLeave: function() {
  },
  onLeaveCompleted: function() {
  }
});
PortfolioSingle.init();


/**
 * Init sliders
 */
function initSliders() {
  $('.slider').each(function() {
    var slider = $(this);
    var sliderCount = slider.prev('.slider-count');
    slider.lightSlider({
          auto:false,
          loop:true,
          item:1,
          mode: 'fade',
          pauseOnHover: false,
          pager: false,
          prevHtml: '<i class="ion-android-arrow-back"></i>',
          nextHtml: '<i class="ion-android-arrow-forward"></i>',
          onBeforeSlide: function (el) {
            console.log(slider.prev('.slider-count'));
              sliderCount.find('.current').text(el.getCurrentSlideCount());
          },
          onSliderLoad: function() {
              slider.removeClass('cS-hidden');
          },
      });
      sliderCount.find('.total').text(slider.getTotalSlideCount());
  });
}

// $(document).ready(function() {
//   initSliders();
// });
Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {
  initSliders();
});