/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.skin = 'minimalist';
	config.toolbarCanCollapse = true;
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Scayt,SelectAll,Find,Replace,Cut,Copy,Paste,PasteText,PasteFromWord,Print,Preview,NewPage,Save,Strike,Subscript,Superscript,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Font,FontSize,BGColor,Maximize,ShowBlocks,About,Styles';

	config.extraPlugins = "lineutils,widget,basewidget,sourcedialog,showprotected";
	config.extraAllowedContent = 'div section span i';
	config.allowedContent = true;
	config.scayt_autoStartup = true;

	// Protect shortcodes
	// config.protectedSource.push(/\[gallery(.+?)?\](?:(.+?)?\[\/gallery\])?/g);
	// config.protectedSource.push(/\[events(.+?)?\](?:(.+?)?\[\/events\])?/g);
	config.protectedSource.push(/\[(.*?)\]/g);
	config.fillEmptyBlocks="&#8203;";  

	/* Filebrowser routes */
	config.filebrowserBrowseUrl = "/ckeditor/attachment_files";
	config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";
	config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";
	config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";
	config.filebrowserImageBrowseUrl = "/ckeditor/pictures";
	config.filebrowserImageUploadUrl = "/ckeditor/pictures";
	config.filebrowserUploadUrl = "/ckeditor/attachment_files";
};

CKEDITOR.dtd.$removeEmpty['span'] = false;
CKEDITOR.dtd.$removeEmpty['i'] = false;

CKEDITOR.on( 'instanceReady', function( ev ) {
    // Ends self-closing tags the HTML4 way, like <br>.
    ev.editor.dataProcessor.writer.selfClosingEnd = '>';

    var writer = ev.editor.dataProcessor.writer;
     
    // The character sequence to use for every indentation step.
    writer.indentationChars = '\t';
     
    // The way to close self closing tags, like <br />.
    writer.selfClosingEnd = ' />';
     
    // The character sequence to be used for line breaks.
    writer.lineBreakChars = '\n';
     
    // The writing rules for the <p> tag.
    writer.setRules( 'div',
        {
            // Indicates that this tag causes indentation on line breaks inside of it.
            indent : true,
     
            // Inserts a line break before the <p> opening tag.
            breakBeforeOpen : true,
     
            // Inserts a line break after the <p> opening tag.
            breakAfterOpen : true,
     
            // Inserts a line break before the </p> closing tag.
            breakBeforeClose : true,
     
            // Inserts a line break after the </p> closing tag.
            breakAfterClose : true
        });

});