// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require jquery_ujs
//= require jquery.easing
//= require barba
//= require materialize-sprockets
//= require velocity/velocity.ui
//= require animations
//= require portfolio
//= require contact
//= require header
//= require select_plugin
//= require lightslider
//= require Scrollify
//= require selectize
//= require ajax_transitions

$(document).ready(function() {
  $('.materialize-select').material_select_with_html();
  $('#btn-nav').sideNav({
      edge: 'left',
      closeOnClick: true,
      draggable: true // Choose whether you can drag to open on touch screens
    }
  );
});