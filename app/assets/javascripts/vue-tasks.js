/** 
 * Selectize dropdown and tags
 */
Vue.component('selectize', {
  template: '<select><slot></slot></select>',
  props: {
    options: { type: Array, default: function () { return [] } },
    multiple: { type: Boolean, default: false },
    value: { twoWay: true, type: Array },
    change: { type: Function },
    // storeObject: { type: Boolean, default: false }
  },
  created: function () {
    var self = this
    var config = {
      debug: true,
      data: self.options,
      // placeholder: self.placeholder
    }
    $(function() {
      if (self.multiple === true) {
        $(self.$el).attr('multiple', true)
      }
      $(self.$el).selectize({
        plugins: ['remove_button'],
        items: [],
        onChange: function (val) {
          self.$emit('input', val)
        }
      });
      $(self.$el).val(self.value).trigger('change')
    });
  }
})

// visibility filters
var filters = {
  all: function (tasks) {
    return tasks
  },
  active: function (tasks) {
    return tasks.filter(function (task) {
      return !task.is_complete
    })
  },
  completed: function (tasks) {
    return tasks.filter(function (task) {
      return task.is_complete
    })
  },
  bySettings: function(tasks) {
    if (app.selectedUsers != null && app.selectedUsers.length > 0) {
      tasks = tasks.filter(function(task) {
          var inArray = false;
          for (var i = 0; i < app.selectedUsers.length; i++) {
            var selectedUser = parseInt(app.selectedUsers[i])
            if ($.inArray(selectedUser, task.user_ids) !== -1) {
              inArray = true;
              break;
            }      
          }
          return inArray;
      })
    }
    if (app.selectedProjects != null && app.selectedProjects.length > 0) {
      tasks = tasks.filter(function(task) {
        if (!task.project_id) return false;
        return $.inArray(task.project_id.toString(), app.selectedProjects) !== -1
      })
    }
    return tasks
  },
}

// app Vue instance
var app = new Vue({
  // app initial state
  data: {
    tasks: [],
    users: [],
    projects: [],
    comments: [],
    selectedUsers: [],
    selectedProjects: [],
    newTask: [],
    currentTask: null,
    currentEditTask: null,
    visibility: 'all'
  },

  created: function() {
      var vm = this;
      this.$http.get('/tasks.json').then(function(response) {
          this.tasks = response.data.tasks ? response.data.tasks : []
          this.users = response.data.users ? response.data.users : []
          this.projects = response.data.projects ? response.data.projects : []
          this.comments = response.data.comments ? response.data.comments : []
      })
      $(window).on("onAfterTaskFormSubmit", function(event, task_id, attribute, value) {
        vm.$set(vm.currentTask, attribute, value);
      });
  },

  // computed properties
  // http://vuejs.org/guide/computed.html
  computed: {
    todayTasks: function () {
      var todaysDate = new Date();
      var todayTasks = []
      todayTasks = this.tasks.filter(function(task) {
          if (!task.end_date) return true
          return new Date(task.end_date) <= todaysDate
      })
      todayTasks = filters['bySettings'](todayTasks)
      return todayTasks
    },
    nextTasks: function () {
      var todaysDate = new Date();
      var nextWeek = new Date();
      nextWeek.setDate(todaysDate.getDate() + 7)
      var nextTasks = []
      nextTasks = this.tasks.filter(function(task) {
          if (task.end_date) {
            var taskDate = new Date(task.end_date)
            return taskDate < nextWeek && taskDate > todaysDate
          } 
          return false
      })
      nextTasks = filters['bySettings'](nextTasks)
      return nextTasks
    },
    visibleTodayTasks: function () {
       return filters[this.visibility](this.todayTasks)
    },
    visibleNextTasks: function () {
       return filters[this.visibility](this.nextTasks)
    },
    remainingToday: function () {
      var remaining = filters.active(this.todayTasks).length
      var completed = filters.completed(this.todayTasks).length
      var total = remaining + completed;
      var progress = completed / total;
      return [
        completed,
        total,
        progress * 100
      ]
    },
    remainingNext: function () {
      var remaining = filters.active(this.nextTasks).length
      var completed = filters.completed(this.nextTasks).length
      var total = remaining + completed;
      var progress = completed / total;
      return [
        completed,
        total,
        progress * 100
      ]
    },
  },

  filters: {
    pluralize: function (n) {
      return n === 1 ? 'task' : 'tasks'
    }
  },

  // methods that implement data logic.
  // note there's no DOM manipulation here at all.
  methods: {
    addTask: function () {
      var name = this.newTask.name && this.newTask.name.trim()
      if (!name) {
        return
      }
      var newTask = {
        name: name,
        is_complete: false,
        user_ids: [],
        project_id: parseInt(this.newTask.project_id)
      }
      this.$http.post('/tasks.json', newTask).then(function(response) {
        var task = response.body
        this.tasks.unshift(task)
        this.newTask = ''
      }, function(error) {
          console.log(error)
      });
    },

    deleteTask: function (task) {
      this.$http.delete('/tasks/' + task.id).then(function(response) {
        this.tasks.splice(this.tasks.indexOf(task), 1)
      }, function(error) {
          console.log(error)
      });
    },

    updateTask: function (task) {
      this.$http.put('/tasks/' + task.id, task).then(function(response) {
      }, function(error) {
          console.log(error)
      });
    },

    openTask: function(task) {
      this.currentTask = task
      showTaskModal(task.id)
    },

    editTask: function (task) {
      console.log('editTask');

      this.beforeEditCache = task.name
      this.currentEditTask = task
    },

    doneEdit: function (task) {
      console.log('done edit');
      if (!this.currentEditTask) {
        return
      }
      this.currentEditTask = null
      task.name = task.name.trim()
      if (!task.name) {
        this.deleteTask(task)
      } else {
        this.updateTask(task)
      }
    },

    cancelEdit: function (task) {
      this.currentEditTask = null
      task.name = this.beforeEditCache
    },

    getUsers: function(task) {
      var ids = task.user_ids.map(function (id) {
          return parseInt(id);
      });
      return this.users.filter(function (user) {
          return ids.indexOf(user.id) > -1;
      });
    },

    getProject: function(task) {
      return this.projects.filter(function (project) {
          return task.project_id == project.id;
      });
    },

  },

  // a custom directive to wait for the DOM to be updated
  // before focusing on the input field.
  // http://vuejs.org/guide/custom-directive.html
  directives: {
    'task-focus': function (el, value) {
      if (value) {
        el.focus()
      }
    }
  }
})

// handle routing
function onHashChange () {
  var visibility = window.location.hash.replace(/#\/?/, '')
  if (filters[visibility]) {
    app.visibility = visibility
  } else {
    window.location.hash = ''
    app.visibility = 'active'
  }
}

window.addEventListener('hashchange', onHashChange)
onHashChange()

// mount
app.$mount('#app')