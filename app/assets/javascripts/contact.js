var Contact = Barba.BaseView.extend({
  namespace: 'contact',
  onEnter: function() {
  },
  onEnterCompleted: function() {
    $('#contact-bg').velocity({
      translateX: 0,
    }, {
      delay: 300,
      duration: 1400,
      complete: function() {

      }
    });
    $('.contact-wrapper').css('visibility', 'visible').velocity({
      opacity: 1,
    },{
      delay: 400,
    });
  },
  onLeave: function() {
  },
  onLeaveCompleted: function() {
  }
});
Contact.init();