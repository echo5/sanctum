/**
 * Open task modal for editing and comments
 */
showTaskModal = function(task_id) {
  var spinner = "<div class=\"valign-wrapper center\">\r\n\t<div class=\"preloader-wrapper active\">\r\n\t  <div class=\"spinner-layer spinner-red-only\">\r\n\t    <div class=\"circle-clipper left\">\r\n\t      <div class=\"circle\"><\/div>\r\n\t    <\/div><div class=\"gap-patch\">\r\n\t      <div class=\"circle\"><\/div>\r\n\t    <\/div><div class=\"circle-clipper right\">\r\n\t      <div class=\"circle\"><\/div>\r\n\t    <\/div>\r\n\t  <\/div>\r\n\t<\/div>\r\n<\/div>";
  var modal = $('#modal-task');
  $.ajax({
      url: '/tasks/' + task_id,
      cache: false,
      dataType: 'html',
      success: function(html){
        modal.html(html);
        modal.find('select').material_select_with_html();
        modal.find('.tooltipped').tooltip({delay: 50});
        initDropzone();
        var datepickers = modal.find('.datepicker');
        datepickers.pickadate({
          format: 'mmm d, yyyy',
          // onStart: function ()
          // {
          //   if (!isNaN(parseInt(this.$node.val()))) {
          //       this.set('select', this.$node.val(), { format: 'yyyy-mm-dd' } );
          //   }
          //   else {
          //       this.clear();
          //   }
          // },
        });

        var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
                  '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

                var emailOptions = $('#email_to').data('options');
        $('#email_to').selectize({
            plugins: ['remove_button'],
            persist: false,
            maxItems: null,
            valueField: 'email',
            labelField: 'name',
            searchField: ['name', 'email'],
            options: emailOptions,
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                        (!item.name && item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                    '</div>';
                },
                option: function(item, escape) {
                  console.log(item);
                    var label = item.name || item.email;
                    var caption = item.name ? item.email : null;
                    return '<div>' +
                        '<span class="label">' + escape(label) + '</span>' +
                        (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
                }
            },
            createFilter: function(input) {
                var match, regex;

                // email@address.com
                regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[0]);

                // name <email@address.com>
                regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[2]);

                return false;
            },
            create: function(input) {
                if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                    return {email: input};
                }
                var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
                if (match) {
                    return {
                        email : match[2],
                        name  : $.trim(match[1])
                    };
                }
                alert('Invalid email address.');
                return false;
            }
        });
      }
  });
  modal.openModal({
      dismissible: true,
      opacity: .5,
      in_duration: 300,
      out_duration: 200,
      starting_top: '0%',
      ending_top: '0%',
      complete: function() {
        $(window).trigger( "closeTaskModal", task_id );
        modal.html(spinner);
      },
    }
  );
};



/**
 * Update task after form changed
 */
$(document).on('change', '.edit_task :input', function() {
  var inputField = $(this),
    form = inputField.closest("form"),
    task_id = form.data('id'),
    attributeName = $(inputField).attr('name'),
    attribute = attributeName.slice(attributeName.indexOf('[') +1,attributeName.indexOf(']')),
    value;

  // Datepicker
  if ($(inputField).hasClass('datepicker')) {
      var picker = $(inputField).pickadate('picker');
      if (picker.get('select') !== null) {
        value = picker.get('select').obj;
      }
  } 
  // Default
  else {
    value = $(inputField).val();
  }

  form.submit();

  $(window).trigger( "onAfterTaskFormSubmit", [task_id, attribute, value] );
});
