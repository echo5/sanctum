$(function(){
	/**
	 *  Change header color on scroll
	 */
	function changeHeaderColorOnScroll() {
		var sectionDark = $('.section-dark');

		if (sectionDark.length) {
			var targetTop = $('.section-dark').offset().top,
				targetBottom = targetTop + $('.section-dark').height()
				body = $('body');

			$(window).scroll(function(){
				// console.log('window: ' + $(window).scrollTop());
				// console.log('targetOffset: ' + targetOffset.top);
			    if ( $(window).scrollTop() >= targetTop && $(window).scrollTop() < targetBottom  ) {   
			        if (!body.hasClass('header-light-active')) {
			    		body.addClass('header-light-active');
			        }
			    } else {
					if (body.hasClass('header-light-active')) {
						body.removeClass('header-light-active');
					}
			    }
			});
		}
	}
	Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {
		changeHeaderColorOnScroll();
	});
});