class MessagesController < ApplicationController
  protect_from_forgery
  after_filter { flash.discard if request.xhr? }

	# POST /messages
	def create
	  @message = Message.new(message_params)
 	  respond_to do |format|
 	    if !params[:website].empty?
 	      format.js {  flash[:error] = "Seems like you're a bot or have incorrectly filled out a field." }
 	    elsif @message.valid?
		  MessageMailer.send_message(message_params).deliver_later
 	      format.js {  flash[:success] = "Your information has been received and we will be in touch shortly!" }
 	    else
 	      format.js {  
 	        if @message.errors
 	          flash[:error] = @message.errors.full_messages.join("<br>") 
 	        else
 	          flash[:error] = "There was an error posting your comment.  Please try again." 
 	        end
 	      }
 	    end
 	  end
	end

	private
	  # Never trust parameters from the scary internet, only allow the white list through.
	  def message_params
	    params.require(:message).permit(:message, :name, :email, :phone)
	  end

end
