class TasksController < ApplicationController
  before_action :set_task, only: [:edit, :update, :destroy]

	protect_from_forgery


  # GET /tasks
  def index
    @users = User.all
    @projects = Project.all
    respond_to do |format|
      format.html
      format.json {
        @tasks = Task.includes(:users, :project).where(task_type: ['task', nil, '']).order(:end_date)
        # @tasks = Task.includes(:users, :project).where(:is_complete => false).where(task_type: ['task', nil]).order(:end_date)
        @task_comments = TaskComment.includes(:user, :task).last(10)
      }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    if params[:id] == 'root'
      # act as if index was called
      data
    else
      @task = Task.includes(:attachments).find(params[:id])
      @task_comment = TaskComment.new
      @task_attachment = TaskAttachment.new
      @task_comments = TaskComment.with_read_marks_for(current_user).includes(:user).where( task_id: @task.id )
      @unread_comments = @task_comments.unread_by_user(current_user).map(&:id)
      @users = User.all
      respond_to do |format|
        if request.xhr?
          format.html   { render layout: false }
        end
        format.html {  }
      end
    end
  end

  def data
    # fetch root tasks (ParentId is null) as json-tree
    tree = Task.get_root_tree
    # render as json
    render :json => tree
  end

  # PATCH/PUT /tasks/update_batch
  # PATCH/PUT /tasks/update_batch.json
  def update_batch
    tasks = params[:tasks]    
    respond_to do |format|
      if Task.update(tasks.keys, tasks.values)
        format.js {  flash[:success] = "Tasks updated" }
        else
        format.js {  flash[:error] = "Error" }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update    
    if @task.update(task_params)
      render :json => @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def create
    @task = Task.create(task_params)
    render :json => JSON::parse(@task.to_json(:include => [:user_ids])).merge("Id" => @task.id)
  end

  def destroy
    @task.destroy
    render :json => {:status => "ok"}
  end

  def reminders
  	# If task is < 1 week away and not passed
  	@upcomingTasks = Task.where('start_date > ? && start_date <= ?', Date.today, Date.today + 7)
  	# Tasks with incompleted dependencies
  	# Incomplete task with deadline
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      # If simple form
      # if params.has_key?(:task)
      #   return params.fetch(:task, {}).permit(:name, :project_id, :start_date, :end_date, :duration, :planned_start, :planned_end, :progress, :sortorder, :task_type, :parent_id, :task_label_id, :is_complete, :in_gantt, :user_ids => [])
      # end
      # Port params from gantt
      # @TODO remove, these are for old gantt system
      if params.has_key?(:text)
        params[:name] = params["text"]
        params.delete :text
      end
      if params.has_key?(:parentId)
        params[:parent_id] = params["parentId"]
        params.delete :parentId
      end
      if params.has_key?(:type)
        params[:task_type] = params["type"]
        params.delete :type
      end
      if params.has_key?(:user_ids) && !params[:user_ids].nil?
        params[:user_ids] = params["user_ids"].split(',')
      end
      params.permit(:name, :project_id, :start_date, :end_date, :duration, :planned_start, :planned_end, :progress, :sortorder, :task_type, :parent_id, :user_ids, :task_label_id, :is_complete, :in_gantt)
    end

end
