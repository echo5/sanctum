class TaskCommentsController < ApplicationController
  before_action :set_task_comment, only: [:show, :edit, :update, :destroy]

  # GET /task_comments/1/edit
  def edit
  end

  # GET /task_comments/unread
  def unread
    @task_comments = TaskComment.unread_by_user(current_user)
    respond_to do |format|
      format.html { render :partial => 'unread' }
      format.js   { render layout: false, content_type: 'text/javascript' }
    end
  end

  # POST /task_comments/mark_as_read
  def mark_as_read
    TaskComment.where(id: params[:ids]).mark_as_read! :all, :for => current_user
  end

  # POST /task_comments
  # POST /task_comments.json
  def create
    @task_comment = TaskComment.new(task_comment_params)
    @task_comment.user_id = current_user.id

    respond_to do |format|
      if @task_comment.save
        if params.has_key?(:email_to) && !params[:email_to].blank?
          TaskMailer.email_comment(@task_comment, params[:email_to]).deliver_now
        end
        format.html { redirect_to @task_comment, notice: 'Task comment was successfully created.' }
        format.js   { render layout: false, content_type: 'text/javascript' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /task_comments/1
  # PATCH/PUT /task_comments/1.json
  def update
    respond_to do |format|
      if @task_comment.update(task_comment_params)
        format.html { redirect_to @task_comment, notice: 'Task comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @task_comment }
      else
        format.html { render :edit }
        format.json { render json: @task_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_comments/1
  # DELETE /task_comments/1.json
  def destroy
    @task_comment.destroy
    respond_to do |format|
      format.html { redirect_to task_comments_url, notice: 'Task comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_comment
      @task_comment = TaskComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_comment_params
      params.fetch(:task_comment, {})
      params.permit(:content, :task_id)
    end
end
