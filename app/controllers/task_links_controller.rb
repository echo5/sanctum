class TaskLinksController < ApplicationController

	protect_from_forgery

	def index
		task_links = TaskComment.includes(:user, :task).last(10)
		render :json => task_links
	end

	 def update
	   link = TaskLink.find(params["id"])
	   link.source = params["source"]
	   link.target = params["target"]
	   link.link_type = params["type"]
	   link.save

	   render :json => {:status => "ok"}
	 end

	 def create
	   link = TaskLink.create :source_id => params["source"], :target_id => params["target"], :link_type => params["type"]
	   render :json => {:tid => link.id}
	 end

	 def destroy
	   TaskLink.find(params["id"]).destroy
	   render :json => {:status => "ok"}
	 end
	
end
