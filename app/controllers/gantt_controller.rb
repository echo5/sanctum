class GanttController < ApplicationController
	def index
	  @resources = User.all
	  # @resources = User.with_role(:resource).preload(:roles)
	  @projects = Project.all
	end

	# def data
	#     tasks = Task.where(:in_gantt => true).ordered.includes(:links, :users)
	#     unread_comments = TaskComment.unread_by(current_user).where.not(:user_id => current_user.id).group("task_id").count
	    
	#     render :json=>{
	#               :data => tasks.flatten.map{|task|{
	#                 :id => task.id,
	#                 :project_id => task.project_id,
	#                 :text => task.name,
	#                 :start_date => task.start_date,
	#                 :duration => task.duration,
	#                 :planned_start => task.planned_start,
	#                 :planned_end => task.planned_end,
	#                 :progress => task.progress,
	#                 :sortorder => task.sortorder,
	#                 :type => task.task_type,
	#                 :task_label_id => task.task_label_id,
	#                 :user_ids => task.users.collect(&:id).flatten,
	#                 :parent => task.parent_id,
	#                 :open => "true",
	#                 :unread_comments => unread_comments[task.id],
	#                 :in_gantt => task.in_gantt
	#               }},
	#               :links => tasks.map(&:links).flatten.map{|link|{
	#                   :id => link.id,
	#                   :source => link.source_id,
	#                   :target => link.target_id,
	#                   :type => link.link_type
	#               }}
	#            }
	# end

	# def index
	#   # fetch root tasks (ParentId is null) as json-tree
	#   tree = Task.get_root_tree
	#   # render as json
	#   render :json => tree
	# end

	def data
	  # fetch root tasks (ParentId is null) as json-tree
	  tree = Task.get_root_tree
	  # render as json
	  render :json => tree
	end

	def show
	  # ExtJS TreeStore requests root node as 'root'
	  if params[:id] == 'root'
	    # act as if index was called
	    data
	  else
	    # fetch single task
	    task = Task.find(params[:id])
	    render :json => task.to_json
	    #render :json => tasks.to_json(:only => [:id, :Name, :StartDate, :Duration, :DurationUnit])
	  end
	end
end
