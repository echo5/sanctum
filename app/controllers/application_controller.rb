class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :store_current_location, :unless => :devise_controller?

  rescue_from CanCan::AccessDenied do |exception| 
    if current_user.nil?
      redirect_to new_user_session_path, :notice => 'Please login'
    else
      if request.referer.present?
        redirect_to :back, :alert => exception.message
      else
        redirect_to root_path, :alert => exception.message
      end
    end
  end

  private
  def store_current_location
    session[:redirect_to] = request.url
    # store_location_for(:user, request.url)
  end

  def after_sign_in_path_for(resource)
    if session[:redirect_to] .present?
      session[:redirect_to] 
    else
      root_path
    end
  end

end

