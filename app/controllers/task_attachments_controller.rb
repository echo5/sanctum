class TaskAttachmentsController < ApplicationController
	protect_from_forgery

	# GET /attachments/:id
	def show
		@task_attachment = TaskAttachment.find(params[:id])
		send_file assets_dir + @task_attachment.file.url(:original, false)
	end

	# GET /tasks/:task_id/attachments
	def index
	  @task_attachments = TaskAttachment.where(params[:task_id])
	end

	# POST /attachments/:task_id
	def create
	@task_attachment = TaskAttachment.new(task_attachment_params)

	  respond_to do |format|
	    if @task_attachment.save
 	  	  format.js {  flash[:success] = "Attachment uploaded successfully" }
	      format.html { redirect_to @task_attachment, notice: 'Attachment was successfully uploaded.' }
	      format.json { render json: @task_attachment, status: :created, location: @task_attachment }
	    else
 	  	  format.js {  flash[:error] = "Error uploading attachment" }
	      format.html { render action: "new" }
	      format.json { render json: @task_attachment.errors, status: :unprocessable_entity }
	    end
	  end
	end

	def destroy
	  @task_attachment = TaskAttachment.find(params[:id])
	  respond_to do |format|
		  if @task_attachment.destroy    
		  	format.js {  flash[:success] = "Attachment deleted" }
		  else
		  	format.js {  flash[:error] = @task_attachment.errors.full_messages.join(',') }
		  end
	  end
	end

	private
		def task_attachment_params
		  params.require(:task_attachment).permit(:file, :task_id)
		end

end
