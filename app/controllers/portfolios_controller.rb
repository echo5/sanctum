class PortfoliosController < ApplicationController
  before_action :set_portfolio, only: [:show, :edit, :update, :destroy]
  after_filter { flash.discard if request.xhr? }
  protect_from_forgery
  layout "frontend"

  # GET /portfolio
  # GET /portfolio.json
  def index
    @portfolio = Portfolio.where(is_published: true)
  end

  # GET /portfolio/1
  # GET /portfolio/1.json
  def show
    if (!@portfolio.is_published)
      authorize! :show, @post
    end
  end

  # GET /portfolio/1/edit
  def edit
    authorize! :edit, @portfolio
    render layout: "live_edit"
  end

  # PATCH/PUT /portfolio/1
  # PATCH/PUT /portfolio/1.json
  def update
    authorize! :edit, @portfolio
    respond_to do |format|
      if @portfolio.update(portfolio_params)
        format.js {  flash[:success] = "Portfolio item updated!" }
        else
        format.js {  flash[:error] = "Error" }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_portfolio
      @portfolio = Portfolio.find_by_slug!(params[:slug])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def portfolio_params
      params.fetch(:portfolio, {})
      params.permit(:content)
    end
end
