class PortfolioTagsController < ApplicationController
  before_action :set_portfolio_tag, only: [:show, :edit, :update, :destroy]

  # GET /portfolio_tags
  # GET /portfolio_tags.json
  def index
    @portfolio_tags = PortfolioTag.all
  end

  # GET /portfolio_tags/1
  # GET /portfolio_tags/1.json
  def show
  end

  # GET /portfolio_tags/new
  def new
    @portfolio_tag = PortfolioTag.new
  end

  # GET /portfolio_tags/1/edit
  def edit
  end

  # POST /portfolio_tags
  # POST /portfolio_tags.json
  def create
    @portfolio_tag = PortfolioTag.new(portfolio_tag_params)

    respond_to do |format|
      if @portfolio_tag.save
        format.html { redirect_to @portfolio_tag, notice: 'Portfolio tag was successfully created.' }
        format.json { render :show, status: :created, location: @portfolio_tag }
      else
        format.html { render :new }
        format.json { render json: @portfolio_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /portfolio_tags/1
  # PATCH/PUT /portfolio_tags/1.json
  def update
    respond_to do |format|
      if @portfolio_tag.update(portfolio_tag_params)
        format.html { redirect_to @portfolio_tag, notice: 'Portfolio tag was successfully updated.' }
        format.json { render :show, status: :ok, location: @portfolio_tag }
      else
        format.html { render :edit }
        format.json { render json: @portfolio_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /portfolio_tags/1
  # DELETE /portfolio_tags/1.json
  def destroy
    @portfolio_tag.destroy
    respond_to do |format|
      format.html { redirect_to portfolio_tags_url, notice: 'Portfolio tag was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_portfolio_tag
      @portfolio_tag = PortfolioTag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def portfolio_tag_params
      params.fetch(:portfolio_tag, {})
    end
end
