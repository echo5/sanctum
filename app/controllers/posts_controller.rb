class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  after_filter { flash.discard if request.xhr? }
  protect_from_forgery
  layout "frontend"
  

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.where(:is_published => true).page params[:page]
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    if (!@post.is_published)
      authorize! :show, @post
    end
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    authorize! :edit, @post
    render layout: "live_edit"
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    authorize! :update, @post
    respond_to do |format|
      if @post.update(post_params)
        format.js {  flash[:success] = "Post updated!" }
        else
        format.js {  flash[:error] = "Error" }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  # def destroy
  #   @post.destroy
  #   respond_to do |format|
  #     format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      if current_user && current_user.has_role?(:admin)
        @post = Post.includes(:user, :category).find_by_slug!(params[:slug])
      else
        @post = Post.includes(:user, :category).find_by_slug!(params[:slug])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.fetch(:post, {})
      params.permit(:content)
    end
end
