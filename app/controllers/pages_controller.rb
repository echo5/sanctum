class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  after_filter { flash.discard if request.xhr? }
  protect_from_forgery
  layout "frontend" 

  # GET /pages/1
  # GET /pages/1.json
  def show
    if (!@page.is_published)
      authorize! :show, @page
    end
  end

  # GET /pages/1/edit
  def edit
    authorize! :edit, @page
    render layout: "live_edit"
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    authorize! :edit, @page
    respond_to do |format|
      if @page.update(page_params)
        format.js {  flash[:success] = "Page updated!" }
        else
        format.js {  flash[:error] = "Error" }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find_by_slug!(params[:slug])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.fetch(:page, {})
      params.permit(:content)
    end
end
