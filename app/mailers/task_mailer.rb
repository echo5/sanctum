class TaskMailer < ApplicationMailer
	default from: "task@sanctumdesigngroup.com"

	def email_comment(comment, emails)
	  mail(to: emails,
	       body: comment.content,
	       content_type: "text/html",
	       subject: comment.task.name)
	end
end
