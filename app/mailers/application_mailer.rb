class ApplicationMailer < ActionMailer::Base
  default from: "info@sanctumdesigngroup.com"
  layout 'mailer'
end
