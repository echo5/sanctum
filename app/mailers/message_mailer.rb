class MessageMailer < ApplicationMailer
	default from: 'info@sanctumdesigngroup.com'

	def send_message(params)
	  @params = params
	  mail(to: 'info@sanctumdesigngroup.com', subject: 'Sanctum Contact Form')
	end
end
