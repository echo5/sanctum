class Project < ActiveRecord::Base

  has_many :tasks, -> { ordered }, :dependent => :destroy
  has_many :links, through: :tasks, :dependent => :destroy
  has_many :gantt_tasks, -> { where(:in_gantt => true ) }, class_name: "Task"

  after_create do
    task = self.tasks.create(
    	name: self.name, 
    	start_date: self.start_date, 
    	duration: self.duration,
    	# planned_start: params["planned_start"],
    	# planned_end: params["planned_end"],
    	task_type: 'project', 
   	);
   	self.root_task_id = task.id 
  end

  def duration
    if self.start_date.present? && self.end_date.present?
      (self.end_date - self.start_date).to_i
    end
  end

end
