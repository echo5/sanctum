class Portfolio < ActiveRecord::Base
	has_attached_file :image, styles: { full: "1920>", medium: "840x360#" }
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	has_and_belongs_to_many :tags, class_name: 'PortfolioTag'
	default_scope { order(order: :asc) }	
end
