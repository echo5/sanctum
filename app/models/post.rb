class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :category, :class_name => 'PostCategory', :foreign_key => 'post_category_id'
  has_attached_file :image, styles: { full: "1920>", medium: "340x600#" }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_presence_of :title, :content
  default_scope { order('created_at DESC') }
end
