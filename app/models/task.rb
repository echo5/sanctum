class Task < ActiveRecord::Base
  belongs_to :project
  belongs_to :task_label
  has_and_belongs_to_many :users
  has_many :links, class_name: "TaskLink", foreign_key: :source_id, :dependent => :destroy
  belongs_to :parent, class_name: 'Task', foreign_key: :parent_id
  has_many :children, class_name: 'Task', foreign_key: :parent_id, :dependent => :destroy
  has_many :comments, class_name: 'TaskComment', foreign_key: :task_id, :dependent => :destroy
  has_many :attachments, class_name: 'TaskAttachment', :dependent => :destroy
  # has_many :comments_with_marks, ->(user) { unread_comments(user) }, class_name: 'TaskComment', foreign_key: :task_id
  TASK_TYPES = { task: 'task', project: 'project', milestone: 'milestone' }
  enum task_type: TASK_TYPES
  scope :ordered, -> { order('sortorder ASC') }
  validates_presence_of :name

  # def end_date
  #   if !self.duration.nil? && !self.start_date.nil?
  #     (self.duration).business_days.after(self.start_date)
  #   end
  # end

  def business_hours_attributes=(*args)
    self.business_hours.clear
    super(*args)
  end

  # root nodes (ParentId is null) as json tree
  def self.get_root_tree
    roots = Task.includes(:children).where(parent_id: nil)
    Task.json(roots)
  end

  # as json with format { children: [ tasks tree ] }
  # to match desired format for ExtJS TreeStore
  def self.json(tasks)
    {
        :success => true,
        :children => Task.json_tree(tasks)
    }
  end

  # as json tree
  def self.json_tree(tasks)
    tasks.map do |task|
      {
          :id => task.id,
          :name => task.name,
          :start_date => task.start_date,
          :duration => task.duration,
          # :DurationUnit => task.DurationUnit,
          :leaf => task.children.count == 0,
          :expanded => true,
          :parentId => task.parent_id,
          :children => task.children.count == 0 ? nil : Task.json_tree(task.children)
      }
    end
  end

end
