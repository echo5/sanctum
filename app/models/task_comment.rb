class TaskComment < ActiveRecord::Base
	validates_presence_of :content, :task_id
	belongs_to :user
	belongs_to :task
	acts_as_readable :on => :created_at
	default_scope { order(created_at: :desc) }
	scope :unread_by_user, ->(user) { where.not(user_id: user.id).unread_by(user) }

end
