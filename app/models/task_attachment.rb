class TaskAttachment < ActiveRecord::Base
	belongs_to :task
	has_attached_file :file,
	  :path => ":rails_root/public/files/task_attachments/:id/:filename",
	  :url  => "/files/task_attachments/:id/:filename",
	  :styles => { thumb: "150x100#" }
	do_not_validate_attachment_file_type :file
	validates_presence_of :file, :task_id

end
