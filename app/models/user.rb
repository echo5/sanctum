class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  rolify
  acts_as_reader
  before_create :set_color
  has_and_belongs_to_many :tasks

  def self.reader_scope
    has_role? :admin
  end

	def gravatar_url(size = 80)
		gravatar_id = Digest::MD5.hexdigest(self.email.downcase)
		"http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=404"
	end

  def set_color
    colors = [
      '#f16364',
      '#f58559',
      '#f9a43e',
      '#e4c62e',
      '#67bf74',
      '#59a2be',
      '#2093cd',
      '#ad62a7',
    ]
    self.color = colors.sample
  end

  def gravatar
    Rails.cache.fetch "user_avatar/#{self.id}", :expires_in => 24.hours do
      uri = URI(self.gravatar_url)
      Net::HTTP.start(uri.host, uri.port) do |http|
        request = Net::HTTP::Head.new uri.request_uri
        response = http.request request
        if response.code.to_i == 200
          gravatar_url
        end
      end
    end
  end

  def avatar(size = 'xsmall')
    avatar_sizes = {
      'xsmall' => 60,
      'small' => 80,
      'medium' => 120,
      'large' => 150
    }
    if !self.gravatar.nil?
      '<img class="avatar avatar-' + size + '" src="' + self.gravatar_url(avatar_sizes[size]) + '" alt="' + self.name + '">'
    else
      '<div class="avatar letter-avatar avatar-' + size + '" style="background-color:' + self.color + ';">' + self.name.split[0,2].map(&:first).join + '</div>'
    end
  end

end
