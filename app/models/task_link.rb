class TaskLink < ActiveRecord::Base
  belongs_to :project  
  belongs_to :source, :class_name => 'Task'
  belongs_to :target, :class_name => 'Task'

end
