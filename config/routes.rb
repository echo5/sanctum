Rails.application.routes.draw do

  # Error handling
  %w(404 422 500).each do |code|
    get code, :to => "errors#show", :code => code
  end

  devise_for :users

  # Rails admin
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  # Ckeditor
  mount Ckeditor::Engine => '/ckeditor'
  
  # Comments
  authenticate :user, ->(user) { user.has_role?(:admin)} do
    get "task_comments/unread", :to => "task_comments#unread", :as => "task_comments_unread"
    post "task_comments/mark_as_read", :to => "task_comments#mark_as_read", :as => "task_comments_mark_as_read"
  end

  # Tasks
  authenticate :user, ->(user) { user.has_role?(:admin)} do
    get "gantt", :to => "gantt#index", :as => "gantt"
    get "gantt/data", :to => "gantt#data", :as => "gantt_data"
    # delete "task/:id", :to => "tasks#delete", :as => "task_delete"
    # get "task/:id/comments", :to => "tasks#comments", :as => "task_get_comments"
    patch "tasks/update_batch", :to => "tasks#update_batch", :as => "task_update_batch"
    resources :users
    resources :gantt
    resources :projects
    resources :task_links
    resources :tasks, :shallow => true, :constraints => { } do
      resources :task_comments, path: 'comments'
      resources :task_attachments, path: 'attachments'
    end
  end

  # Users
  devise_scope :user do
    get "/signup" => "devise/registrations#new"
    # post '/signup' => 'registrations#new', :as => :new_user_registration 
    get "/profile/edit" => "devise/registrations#edit"
    get "/login" => "devise/sessions#new", :as => :login
    get "/logout" => "devise/sessions#destroy"
  end

  # Root
  root to: "pages#show", :slug => "home"

  # Posts
  resources :posts, path: 'blog', param: :slug
  # get '/blog' => 'posts#index', :as => 'blog'
  # get '/blog/:slug' => 'posts#show', :as => 'post'
  # get '/blog/:slug/edit' => 'posts#edit', :as => 'edit_post'
  # patch '/blog/:slug/update' => 'posts#update', :as => 'update_post'

  # Portfolio
  get '/portfolio' => 'portfolios#index', :as => 'portfolio'
  get '/portfolio/:slug' => 'portfolios#show', :as => 'show_portfolio'
  get '/portfolio/:slug/edit' => 'portfolios#edit', :as => 'edit_portfolio'
  patch '/portfolio/:slug/update' => 'portfolios#update', :as => 'update_portfolio'

  # Contact / messages
  post '/contact' => 'messages#create', :as => 'messages'

  # Pages
  get '/' => 'pages#show', :slug => 'home', :as => 'home'
  get '/:slug/edit' => 'pages#edit', :as => 'edit_page', :constraints => {:slug => /.*/}
  patch '/:slug/update' => 'pages#update', :as => 'update_page', :constraints => {:slug => /.*/}
  get '/:slug' => 'pages#show', :constraints => {:slug => /.*/}, :as => 'page'


end
