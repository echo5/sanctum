# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://sanctumdesigngroup.com"

SitemapGenerator::Sitemap.create do
  # Add pages
  Page.where(is_published: true).find_each do |page|
    # Skip home- already included in sitemap
    if page.slug != 'home'
      add CGI::unescape(page_path(page.slug)), :lastmod => page.updated_at
    end
  end

  # Add '/blog'
  add posts_path, :priority => 0.7, :changefreq => 'daily'

  # Add all posts:
  Post.where(is_published: true).find_each do |post|
      add CGI::unescape(post_path(post.slug)), :lastmod => post.updated_at
  end

  # Add '/portfolio'
  add portfolio_path, :priority => 0.7, :changefreq => 'daily'

  # Add all portfolio items:
  Portfolio.where(is_published: true).find_each do |portfolio_item|
    add CGI::unescape(show_portfolio_path(portfolio_item.slug)), :lastmod => portfolio_item.updated_at
  end
end
