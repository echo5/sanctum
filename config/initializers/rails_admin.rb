require Rails.root.join('lib', 'rails_admin', 'gantt.rb')
RailsAdmin::Config::Actions.register(RailsAdmin::Config::Actions::Gantt)

RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.authorize_with do |controller|
    redirect_to '/login' unless current_user && current_user.has_role?(:admin)
  end

  config.actions do
    dashboard
    gantt
    index
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  #Portfolio
  config.model Portfolio do

    configure :live_edit do
      pretty_value do
        portfolio = bindings[:object]
        %{<div class="live-edit">
            <a href="/portfolio/#{portfolio.slug}/edit">Live Edit</a>
          </div >}.html_safe
      end
      read_only true
    end

    edit do
      field :title
      field :slug
      field :description
      field :live_edit
      field :content, :ck_editor
      field :image
      field :is_published
      field :order
    end
  end

  #Posts
  config.model 'Post' do

    configure :live_edit do
      pretty_value do
        page = bindings[:object]
        %{<div class="live-edit">
            <a href="/blog/#{page.slug}/edit">Live Edit</a>
          </div >}.html_safe
      end
      read_only true
    end

    list do
      field :title
      field :slug
      field :image
      field :created_at
      field :is_published
    end

    edit do
      field :title
      field :slug
      field :description
      field :image
      field :content
      field :live_edit
      field :created_at
      field :category
      field :is_published
      field :user
    end

  end

  # Pages
  config.model Page do

    configure :live_edit do
      pretty_value do
        page = bindings[:object]
        %{<div class="live-edit">
            <a href="/#{page.slug}/edit">Live Edit</a>
          </div >}.html_safe
      end
      read_only true
    end

    edit do
      field :title
      field :slug
      field :description
      field :live_edit
      field :content
      field :is_published
    end

  end

  # config.model User do
  #   edit do
  #     field :name
  #     field :email
  #     field :password, :password do
  #       label "Password"
  #     end
  #   end
  # end

end
