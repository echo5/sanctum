ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  address:              "one.mxroute.com",
  port:                 "465",
  user_name:            ENV['MAIL_USER'],
  password:             ENV['MAIL_PASS'],
  domain:               "sanctumdesigngroup.com",
  authentication:       :plain,
  enable_starttls_auto: true,
  ssl:                   true,
  tls:                   true
}